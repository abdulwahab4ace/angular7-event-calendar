import { Component, OnInit, Input } from '@angular/core';
import { Day } from '../day';
import { DataSyncService } from '../data-sync.service';

@Component({
  selector: 'app-month-view',
  templateUrl: './month-view.component.html',
  styleUrls: ['./month-view.component.css']
})
export class MonthViewComponent implements OnInit {

  @Input() view: boolean;
  @Input() year: number;
  @Input() month: number;
  @Input() months: Array<string>;
  @Input() days: Array<string>;
  @Input() dayNum: Array<Day>;
  @Input() weeks: [];

  public currentDate = new Date();
  public cDate: number = this.currentDate.getDate();
  public cMonth: number = this.currentDate.getMonth();
  public cYear: number = this.currentDate.getFullYear();


  //service
  public message: string;

  constructor(private data: DataSyncService) { }

  ngOnInit() {

    this.data.currentMessage.subscribe(
      message => this.message = message
    );
  }

}
