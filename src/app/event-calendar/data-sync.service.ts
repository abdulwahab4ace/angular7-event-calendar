import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Day } from './day';

@Injectable({
  providedIn: 'root'
})
export class DataSyncService {

  private messageSource = new BehaviorSubject<string>("default message");
  public currentMessage = this.messageSource.asObservable();

  
  public dayType: Day = {
    dayName:new Date().getDay(),
    date:new Date().getDate(),
    monthNum:new Date().getMonth()
  };
  private daySource = new BehaviorSubject<Day>(this.dayType);
  public selectedDate = this.daySource.asObservable();

  constructor() { }

  changeMessage(message: string){
    this.messageSource.next(message);
  }

  changeDay(day: Day){
    this.daySource.next(day);
  }
}
