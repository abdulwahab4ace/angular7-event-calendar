import { Component, OnInit, ViewChild } from '@angular/core';
import { Day } from './day';
import { WeekViewComponent } from './week-view/week-view.component';
import { DayViewComponent } from './day-view/day-view.component';
import { DataSyncService } from './data-sync.service';

@Component({
  selector: 'app-event-calendar',
  templateUrl: './event-calendar.component.html',
  styleUrls: ['./event-calendar.component.css']
})
export class EventCalendarComponent implements OnInit {

  @ViewChild(WeekViewComponent)
  private weekAccess: WeekViewComponent;

  @ViewChild(DayViewComponent)
  private dayAccess: DayViewComponent;

  public dayNum: Array<Day>;
  public months: Array<string> = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  public days: Array<string> = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  //store selected month or year by default current selected
  public month: number;
  public year: number;

  //store range of years
  public years = [];

  //store weeks of current selected month
  public weeks = [];

  public currentDate = new Date();

  /** Switch b/w views */
  public monthView: boolean = true;
  public weekView: boolean = true;
  public dayView: boolean = true;

  //service share
  public message: string;

  constructor(private data: DataSyncService) { }

  ngOnInit() {

    //service
    this.data.currentMessage.subscribe(
      message => this.message = message
    );

    this.month = this.currentDate.getMonth();
    this.year = this.currentDate.getFullYear();

    this.dayNum = this.getDaysInMonth(this.month, this.year);
    this.breakIntoWeeks();

    this.yearsRange(1980, 2050);
  }

  changeIt(){
    this.data.changeMessage("Abdul Wahab");
  }

  check(){
  }

  /**
   * @param int The month number, 0 to 11
   * @param int The year
   * @return Day type
   */
  getDaysInMonth(month, year) {
    const date = new Date(year, month, 1);
    const day: Array<Day> = [];
    if (date.getDay() !== 0) {
      const totalDays = this.daysInPreviousMonth(month, year);
      const fillDate = new Date(year, month - 1, totalDays - date.getDay() + 1);

      for (let i = 0; i < date.getDay(); i++) {
        day.push({date: fillDate.getDate(), dayName: fillDate.getDay(), monthNum: fillDate.getMonth()});
        fillDate.setDate(fillDate.getDate() + 1);
      }
    }
    month = Number(month);
    while (date.getMonth() === month) {
      day.push({date: date.getDate(), dayName: date.getDay(), monthNum: date.getMonth()});
      date.setDate(date.getDate() + 1);
    }
    
    const needMore = 7 - date.getDay();
    for (let i = 0; i < needMore ; i++) {
      day.push({date: date.getDate(), dayName: date.getDay(), monthNum: date.getMonth()});
      date.setDate(date.getDate() + 1);
    }
    return day;
  }

  /**
   * get total days in previous month
   * use to fill the void before active month
   * @param month 
   * @param year 
   */
  private daysInPreviousMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }

  /**
   * quickly show next month
   */
  nextMonth() {
    this.month++;
    if(this.month === 12){
      this.year++;
      this.month = 0;
    }
    this.updateAll();
  }

  /**
   * quickly show previous month
   */
  previousMonth() {
    this.month--;
    if(this.month === -1){
      this.year--;
      this.month = 11;
    }
    this.updateAll();
  }

  /**
   * Always call after getDaysInMonth(month, year);
   */
  private breakIntoWeeks(){
    this.weeks = [];
    for(let w = 0; w < this.dayNum.length / 7; w++){

      const oneWeek = [];
      for(let d = 0; d < 7; d++){
        oneWeek.push(this.dayNum[d + (w * 7)]);
      }
      this.weeks.push(oneWeek);
    }
  }

  /**
   * Years range to select in view
   * @param start 
   * @param end 
   */
  yearsRange(start: number, end: number){
    for(let y = start; y <= end; y++){
      this.years.push(y);
    }
  }

  /**
   * always call when select month or year
   */
  changeYearMonth(){
    this.month = Number(this.month);
    this.year = Number(this.year);
    this.dayNum = this.getDaysInMonth(this.month, this.year);
    this.breakIntoWeeks();
  }

  /**
   * 
   * @param view 
   * portable routing b/w calendar views
   */
  switchView(view: string){
    switch (view) {
      case 'm':
        this.monthView = true;
        this.weekView = false;
        this.dayView = false;
        break;

      case 'w':
        this.monthView = false;
        this.weekView = true;
        this.dayView = false;
        break;

      case 'd':
        this.monthView = false;
        this.weekView = false;
        this.dayView = true;
        break;
    
      default:
        break;
    }
  }

  updateChildWeek(){
    this.weekAccess.cWeek = this.weeks[this.weekAccess.cNumWeek];
    if(!this.weekAccess.cWeek){
      this.weekAccess.cWeek = this.weeks[this.weekAccess.cNumWeek - 1];
    }
  }

  updateChildDay_dayNum(){
    this.dayAccess.dayNum = this.dayNum;
    this.dayAccess.month = this.month;
    // this.dayAccess.year = this.year;
  }

  changeYearMonthWeek(){
    this.updateAll();
  }

  updateAll(){
    this.changeYearMonth();
    this.updateChildWeek();
    this.updateChildDay_dayNum();
    this.dayAccess.parentDayUpdate();
  }

  // /**
  //  * Exclusive for DayComponent quickly show next month
  //  */
  // nextMonth_DayComp() {
  //   this.month++;
  //   if(this.month === 12){
  //     this.year++;
  //     this.month = 0;
  //   }
  //   this.updateAll_DayComp();
  // }

  // /**
  //  * Exclusive for DayComponent quickly show previous month
  //  */
  // previousMonth_DayComp() {
  //   this.month--;
  //   if(this.month === -1){
  //     this.year--;
  //     this.month = 11;
  //   }
  //   this.updateAll_DayComp();
  // }
  // /**
  //  * Exclusive for DayComponent 
  //  */ 
  // updateAll_DayComp(){
  //   this.changeYearMonth();
  //   this.updateChildWeek();
  //   this.updateChildDay_dayNum();
  // }
}
