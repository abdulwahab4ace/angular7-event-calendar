import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Day } from '../day';
import { DataSyncService } from '../data-sync.service';

@Component({
  selector: 'app-day-view',
  templateUrl: './day-view.component.html',
  styleUrls: ['./day-view.component.css']
})
export class DayViewComponent implements OnInit {

  @Input() view: boolean;
  @Input() year: number;
  @Input() month: number;
  @Input() months: Array<string>;
  @Input() days: Array<string>;
  @Input() dayNum: Array<Day>;
  @Input() weeks: [];

  
  @Output() nextMonthEvent: EventEmitter<any> = new EventEmitter();
  @Output() previousMonthEvent: EventEmitter<any> = new EventEmitter();
  
  public currentDate = new Date();
  public cDate: number = this.currentDate.getDate();
  public cMonth: number = this.currentDate.getMonth();
  public cYear: number = this.currentDate.getFullYear();
  
  public selectedDate: Day;
  public indexDate: number;

  //service share
  public message: string;
  public day: Day;

  constructor(private data: DataSyncService) { }

  ngOnInit() {
    //service
    this.data.currentMessage.subscribe(
      message => this.message = message
    );
    
    this.indexDate = this.dayNum.findIndex(x => x.date == this.cDate);
    this.selectedDate = this.dayNum[this.indexDate];
  }

  nextDay(){
    let index = this.dayNum.findIndex(x => x.date == this.daysInMonth(this.month,this.year));
    if(index < 10){
      let rev_dayNum: Array<Day> = this.dayNum; 
      let revIndex = rev_dayNum.reverse().findIndex(x => x.date == this.daysInMonth(this.month,this.year));
      let totalIndex  = rev_dayNum.length;

      let endIndex = totalIndex - 1 - revIndex;
      rev_dayNum = rev_dayNum.reverse();
      if(this.indexDate < endIndex){
        this.selectedDate = this.dayNum[++this.indexDate];
        return;
      }
      this.nextMonth();
      this.indexDate = this.dayNum.findIndex(x => x.date == 1);
      this.selectedDate = this.dayNum[this.indexDate]; 
    }else{
      if(this.indexDate < index){
        this.selectedDate = this.dayNum[++this.indexDate];
        return;
      }
      this.nextMonth();
      this.indexDate = this.dayNum.findIndex(x => x.date == 1);
      this.selectedDate = this.dayNum[this.indexDate]; 
    }
  }
  nextMonth(){
    this.nextMonthEvent.emit();
  }
  changeDayForWeek(){
    this.data.changeDay(this.selectedDate);
  }

  previousDay(){
    let index = this.dayNum.findIndex(x => x.date == 1);
    if(this.indexDate > index){
      this.selectedDate = this.dayNum[--this.indexDate];
      return;
    }
    this.previousMonth();
    
    if(this.dayNum.findIndex(x => x.date == this.daysInMonth(this.month,this.year)) > 10){
      this.indexDate = this.dayNum.findIndex(x => x.date == this.daysInMonth(this.month,this.year));
      this.selectedDate = this.dayNum[this.indexDate];
      return;
    }
    let rev_dayNum: Array<Day> = this.dayNum;
    let revIndex = rev_dayNum.reverse().findIndex(x => x.date == this.daysInMonth(this.month,this.year));
    let totalIndex  = rev_dayNum.length;
    this.indexDate = totalIndex - 1 - revIndex;
    rev_dayNum = rev_dayNum.reverse();
    this.selectedDate = this.dayNum[this.indexDate];
  }
  previousMonth(){
    this.previousMonthEvent.emit();
  }

  /**
   * get total days in previous month
   * use to fill the void before active month
   * @param month 
   * @param year 
   */
  private daysInMonth (month, year) {
    return new Date(year, month+1, 0).getDate();
  }


  /**
   * For only parent component
   * Never calls this method by its own component
   * Method only to be called by parent component when next & previous month is in action
   */
  parentDayUpdate(){
    this.indexDate = this.dayNum.findIndex(x => x.date == this.selectedDate.date);
    if(this.selectedDate.date > 20 && this.indexDate < 10){
      let rev_dayNum: Array<Day> = this.dayNum.reverse();
      let revIndex = rev_dayNum.findIndex(x => x.date == this.daysInMonth (this.month, this.year));
      let totalIndex  = rev_dayNum.length;
      rev_dayNum = rev_dayNum.reverse();
      let newIndex = totalIndex - 1 - revIndex;
      this.indexDate = newIndex;
      this.selectedDate = this.dayNum[this.indexDate];
      return;
    }
    this.selectedDate = this.dayNum[this.indexDate];
  }
}