import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Day } from '../day';
import { DataSyncService } from '../data-sync.service';

@Component({
  selector: 'app-week-view',
  templateUrl: './week-view.component.html',
  styleUrls: ['./week-view.component.css']
})
export class WeekViewComponent implements OnInit {

  @Input() view: boolean;
  @Input() year: number;
  @Input() month: number;
  @Input() months: Array<string>;
  @Input() days: Array<string>;
  @Input() dayNum: Array<Day>;
  @Input() weeks: [];

  @Output() nextWeekEvent: EventEmitter<any> = new EventEmitter();
  @Output() previousWeekEvent: EventEmitter<any> = new EventEmitter();

  public currentDate = new Date();
  public cDate: number = this.currentDate.getDate();
  public cMonth: number = this.currentDate.getMonth();
  public cYear: number = this.currentDate.getFullYear();

  //contains current week information
  public cWeek: Array<Day>;

  //contains current week index
  public cNumWeek: number;
 
  //service share
  public message: string;
  public day: Day;
 
  constructor(
    private data: DataSyncService,
    ) { }

  ngOnInit() {

    //service
    this.data.currentMessage.subscribe(
      message => this.message = message
    );

    //service
    this.data.selectedDate.subscribe(
        (day) => {
          this.day = day;
          
          setTimeout(() => {
            const index = this.cWeek.findIndex(x => x.date == day.date);
            console.log(index)
          }, 1);
        });

    
    let index = this.dayNum.findIndex(x => x.date == this.cDate);   
    this.cWeek = this.weeks[Math.floor(index/7)];
    this.cNumWeek = Math.floor(index/7);
  }

  nextWeek(){
    let numWeeks = this.weeks.length;
    if(this.cNumWeek <  numWeeks - 1){
      this.cWeek = this.weeks[++this.cNumWeek];
      return;
    }
    this.cNumWeek = 0;
    this.nextMonth();
  }

  nextMonth(){
    this.nextWeekEvent.emit();
  }

  previousWeek(){
    let numWeeks = this.weeks.length;
    if(this.cNumWeek >  0){
      this.cWeek = this.weeks[--this.cNumWeek];
      return;
    }
    this.cNumWeek = numWeeks - 1;
    this.previousMonth();
  }

  previousMonth(){
    this.previousWeekEvent.emit();
  }

  outside(){
    
  }
}
