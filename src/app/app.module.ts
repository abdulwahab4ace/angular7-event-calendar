import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventCalendarComponent } from './event-calendar/event-calendar.component';
import { MonthViewComponent } from './event-calendar/month-view/month-view.component';
import { WeekViewComponent } from './event-calendar/week-view/week-view.component';
import { DayViewComponent } from './event-calendar/day-view/day-view.component';

@NgModule({
  declarations: [
    AppComponent,
    EventCalendarComponent,
    MonthViewComponent,
    WeekViewComponent,
    DayViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
